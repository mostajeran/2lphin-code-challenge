package models

import (
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
)

type Trip struct {
	Id         int16
	StartPoint client_events.Point
	EndPoint   client_events.Point
}
