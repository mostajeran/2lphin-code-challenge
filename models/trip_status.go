package models

type TripStatus struct {
	Trip
	Taxi
	Success bool
}
