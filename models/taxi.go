package models

import (
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
)

type Taxi struct {
	Id              int16
	DriverName      string
	DriverPhone     string
	CurrentLocation *client_events.Point
	Rate            float32
}

func (t Taxi) ToProtoModel() *client_events.Taxi {
	return &client_events.Taxi{
		DriverName:  t.DriverName,
		DriverPhone: t.DriverPhone,
		Location:    t.CurrentLocation,
	}
}
