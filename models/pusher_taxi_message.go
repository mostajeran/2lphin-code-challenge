package models

import (
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
)

type PusherTaxiMessage struct {
	TaxiId      int16
	DispatchReq client_events.DispatchingRequest
}
