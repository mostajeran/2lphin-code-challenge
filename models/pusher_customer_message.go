package models

import (
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
)

type PusherCustomerMessage struct {
	CustomerId   int16
	TripResponse client_events.TripResponse
}
