package main

import (
	"context"

	"gitlab.com/mostajeran/2lphin-code-challenge/db"
	"gitlab.com/mostajeran/2lphin-code-challenge/socket_server"
)

const (
	connHost = "localhost"
	connPort = "8080"
	connType = "tcp"
)

func main() {
	socket_server.InitSocketServer(connHost, connPort, connType)
	defer db.Db.Close(context.Background())
}
