module gitlab.com/mostajeran/2lphin-code-challenge

go 1.14

require (
	github.com/jackc/pgx/v4 v4.11.0
	google.golang.org/protobuf v1.26.0
)
