-- Table: public.taxis

-- DROP TABLE public.taxis;

CREATE TABLE public.taxis
(
    id integer NOT NULL DEFAULT nextval('taxis_id_seq'::regclass),
    driver_name character varying(50) COLLATE pg_catalog."default",
    driver_phone character varying(11) COLLATE pg_catalog."default",
    current_location geography,
    rate double precision,
    CONSTRAINT taxis_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;
``
ALTER TABLE public.taxis
    OWNER to postgres;