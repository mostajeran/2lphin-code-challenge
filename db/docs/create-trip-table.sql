-- Table: public.trips

-- DROP TABLE public.trips;

CREATE TABLE public.trips
(
    id integer NOT NULL DEFAULT nextval('trips_id_seq'::regclass),
    start_point geography,
    end_point geography,
    taxi_id integer,
    accepted boolean,
    CONSTRAINT trips_pkey PRIMARY KEY (id),
    CONSTRAINT trips_taxi_id_fkey FOREIGN KEY (taxi_id)
        REFERENCES public.taxis (id) MATCH SIMPLE
        ON UPDATE SET NULL
        ON DELETE SET NULL
)

TABLESPACE pg_default;

ALTER TABLE public.trips
    OWNER to postgres;