package repositories

import (
	"context"

	"gitlab.com/mostajeran/2lphin-code-challenge/db"
	"gitlab.com/mostajeran/2lphin-code-challenge/models"
)

// distance in meters
func FindNearTaxies(lng, lat, distance float64) ([]models.Taxi, []error) {
	results := []models.Taxi{}
	resultErrors := []error{}
	rows, err := db.Db.Query(context.Background(), `
		select id, driver_name, driver_phone, current_location, rate from taxis 
		where ST_Distance(current_location, ST_MakePoint($1, $2)) < $3 limit 10`, lng, lat, distance)
	if err != nil {
		return []models.Taxi{}, []error{err}
	}
	for rows.Next() {
		tempRow := models.Taxi{}
		err := rows.Scan(&tempRow.DriverName, &tempRow.DriverPhone, &tempRow.CurrentLocation, &tempRow.Rate)
		if err != nil {
			resultErrors = append(resultErrors, err)
			continue
		}
		results = append(results, tempRow)
	}
	return results, resultErrors
}
