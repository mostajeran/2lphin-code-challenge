package socket_server

import (
	"log"
	"net"

	"gitlab.com/mostajeran/2lphin-code-challenge/models"
	"google.golang.org/protobuf/proto"
)

func pusherForTaxis(messages <-chan models.PusherTaxiMessage) {
	for {
		message := <-messages
		connInterface, ok := taxiesSocketStorage.Load(message.TaxiId)
		if !ok {
			log.Println("taxi conn not available")
			continue
		}
		conn := connInterface.(net.Conn)

		payloadBuffer, err := proto.Marshal(&message.DispatchReq)
		if err != nil {
			log.Println(err)
			continue
		}

		// TODO add 4bytes on first buffer for MessageType
		_, err = conn.Write(payloadBuffer)
		if err != nil {
			log.Println(err)
		}
	}
}

func pusherForCustomers(messages <-chan models.PusherCustomerMessage) {
	for {
		message := <-messages
		connInterface, ok := customersSocketStorage.Load(message.CustomerId)
		if !ok {
			log.Println("customer conn not available")
			continue
		}
		conn := connInterface.(net.Conn)

		payloadBuffer, err := proto.Marshal(&message.TripResponse)
		if err != nil {
			log.Println(err)
			continue
		}

		// TODO add 4bytes on first buffer for MessageType
		_, err = conn.Write(payloadBuffer)
		if err != nil {
			log.Println(err)
		}
	}
}
