package socket_server

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"os"
	"sync"

	"gitlab.com/mostajeran/2lphin-code-challenge/models"
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
	"gitlab.com/mostajeran/2lphin-code-challenge/services"
	"google.golang.org/protobuf/proto"
)

var (
	// id int16: conn net.conn
	customersSocketStorage sync.Map
	taxiesSocketStorage    sync.Map

	pusherTaxiMessagesChannel     = make(chan models.PusherTaxiMessage)
	pusherCustomerMessagesChannel = make(chan models.PusherCustomerMessage)

	tripRequestsQueue     = make(chan client_events.TripRequest)
	dispatchResponseQueue = make(chan client_events.DispatchingResponse)
)

// TODO reaserch for when to use close(ch) in this project

func InitSocketServer(connHost, connPort, connType string) {
	l, err := net.Listen(connType, connHost+":"+connPort)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	log.Println("server is running on port 8080")
	defer l.Close()

	go services.TripRequest(tripRequestsQueue, pusherTaxiMessagesChannel, pusherCustomerMessagesChannel)
	go services.DispatchingResponse(dispatchResponseQueue, pusherTaxiMessagesChannel, pusherCustomerMessagesChannel)
	go pusherForCustomers(pusherCustomerMessagesChannel)
	go pusherForTaxis(pusherTaxiMessagesChannel)

	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println("Error connecting:", err.Error())
			return
		}
		fmt.Println("Client connected.")

		fmt.Println("Client " + c.RemoteAddr().String() + " connected.")

		go handleConnection(c)
	}
}

func handleConnection(conn net.Conn) {
	var payloadBuffer = make([]byte, 4096)
	n, err := conn.Read(payloadBuffer)
	if err != nil {
		log.Println(err.Error())
		conn.Write([]byte("wrong data encode!"))
	}

	MessageType, err := binary.ReadVarint(bytes.NewBuffer(payloadBuffer[:4]))
	if err != nil {
		conn.Write([]byte("wrong data encode!"))
	}
	payload := payloadBuffer[4:n]

	switch MessageType {
	case int64(client_events.MessageType_TRIP_REQUEST):

		var tripRequestPayload client_events.TripRequest
		err := proto.Unmarshal(payload, &tripRequestPayload)
		if err != nil {
			conn.Write([]byte(err.Error()))
		}

		tripRequestsQueue <- tripRequestPayload

	case int64(client_events.MessageType_DISPATCHING_RESPONSE):

		var dispatchingResponsePayload client_events.DispatchingResponse
		err := proto.Unmarshal(payload, &dispatchingResponsePayload)
		if err != nil {
			conn.Write([]byte(err.Error()))
		}

		dispatchResponseQueue <- dispatchingResponsePayload

	default:
		conn.Write([]byte("wrong request event!"))
	}

	handleConnection(conn)
}
