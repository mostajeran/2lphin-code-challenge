package services

import (
	"log"
	"sort"
	"sync"
	"time"

	"gitlab.com/mostajeran/2lphin-code-challenge/models"
	"gitlab.com/mostajeran/2lphin-code-challenge/protobuf/client_events"
	"gitlab.com/mostajeran/2lphin-code-challenge/repositories"
)

// tripId int16: accept chan bool
var taxiAcceptanceChannelHolder sync.Map

func TripRequest(
	inCh <-chan client_events.TripRequest,
	taxiPusher chan<- models.PusherTaxiMessage,
	customerPusher chan<- models.PusherCustomerMessage) {

	req := <-inCh
	nearTaxies, errors := repositories.FindNearTaxies(req.StartPoint.Longitude, req.StartPoint.Latitude, 10000)
	if len(errors) > 0 {
		for _, e := range errors {
			log.Println("repository error: ", e.Error())
		}
	}

	// notifying customer that taxi not found
	if len(nearTaxies) < 1 {
		customerPusher <- models.PusherCustomerMessage{
			CustomerId: 1, // TODO customerId trip.customer.id
			TripResponse: client_events.TripResponse{
				TripResponse: client_events.TripResponseCode_TripResponseCode_TAXI_NOT_FOUND,
			},
		}
	}

	go func() {
		sort.Slice(nearTaxies, func(i, j int) bool {
			return nearTaxies[i].Rate < nearTaxies[j].Rate
		})

		if len(nearTaxies) > 5 {
			nearTaxies = nearTaxies[:5]
		}

		go dispatchHandler(models.Trip{
			Id:         1, // TODO change it
			StartPoint: *req.StartPoint,
			EndPoint:   *req.EndPoint,
		},
			nearTaxies,
			taxiPusher,
			customerPusher)
	}()
}

func dispatchHandler(
	trip models.Trip,
	nearTaxies []models.Taxi,
	taxiPusher chan<- models.PusherTaxiMessage,
	customerPusher chan<- models.PusherCustomerMessage) {

	accept := make(chan bool)
	taxiAcceptanceChannelHolder.Store(trip.Id, accept)
	go func() {
		taxiPusher <- models.PusherTaxiMessage{
			TaxiId: 1, // TODO change it to nearTaxies[0].id
			DispatchReq: client_events.DispatchingRequest{
				EndPoint:   &trip.EndPoint,
				StartPoint: &trip.StartPoint,
			},
		}
		var accepted bool
		for i := 1; i < len(nearTaxies); i++ {
			select {
			case b := <-accept:
				accepted = b

			case <-time.After(30 * time.Second):
				accepted = false
			}
			if !accepted {
				taxiPusher <- models.PusherTaxiMessage{
					TaxiId: 1, // TODO change it to nearTaxies[i].id
					DispatchReq: client_events.DispatchingRequest{
						StartPoint: &trip.StartPoint,
						EndPoint:   &trip.EndPoint,
					},
				}
			} else {
				customerPusher <- models.PusherCustomerMessage{
					CustomerId: 1, // TODO change it trip.customer.id
					TripResponse: client_events.TripResponse{
						Taxi:         nearTaxies[i-1].ToProtoModel(),
						TripResponse: client_events.TripResponseCode_TripResponseCode_OK,
					},
				}
				break
			}
		}
		if !accepted {
			customerPusher <- models.PusherCustomerMessage{
				CustomerId: 1, // TODO customerId trip.customer.id
				TripResponse: client_events.TripResponse{
					TripResponse: client_events.TripResponseCode_TripResponseCode_TAXI_NOT_FOUND,
				},
			}
			taxiAcceptanceChannelHolder.Delete(trip.Id)
		}
	}()
}

func DispatchingResponse(
	inCh <-chan client_events.DispatchingResponse,
	taxiPusher chan<- models.PusherTaxiMessage,
	customerPusher chan<- models.PusherCustomerMessage) {

	res := <-inCh

	tripId := 1 // TODO change it
	acceptInterface, exists := taxiAcceptanceChannelHolder.Load(tripId)
	if !exists {
		log.Println("channel not found")
		return
	}
	accept := acceptInterface.(chan bool)

	if res.ResponseCode == client_events.DispatchingResponseCode_DispatchingResponseCode_ACCEPTE {
		accept <- true
	} else {
		accept <- false
	}
}
